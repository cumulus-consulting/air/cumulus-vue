module.exports = {
    root: true,
    env: {
        node: true,
    },
    extends: [
        'plugin:vue/essential',
        '@vue/airbnb',
    ],
    parserOptions: {
        parser: 'babel-eslint',
    },
    plugins: [
        'mocha-no-only',
    ],
    rules: {
        'arrow-parens': ['error', 'as-needed'],
        'import/no-extraneous-dependencies': ['error', { devDependencies: ['**/*.spec.js'] }],
        indent: ['error', 4],
        'mocha-no-only/mocha-no-only': ['error'],
        'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        'no-param-reassign': ['error', { props: false }],
        'no-unused-expressions': 0,
    },
    overrides: [
        {
            files: [
                '**/components/**/*.spec.js',
                '**/tests/unit/**/*.spec.{j,t}s?(x)',
            ],
            env: {
                mocha: true,
            },
        },
    ],
};
