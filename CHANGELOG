# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.6.2] - 2021-09-28
### Added
- `dropdownClosed` and `dropdownOpened` events
### Changed
- `dropdownValueChanged` now includes the Event object as an extra argument in its payload

## [1.6.1] - 2021-09-22
### Changed
- "Select All" button moved from bottom of dropdown to top of dropdown

## [1.6.0] - 2021-05-26
### Added
- Multiselect dropdowns
- Custom classes for dropdown options

## [1.5.0] - 2021-04-01
### Added
- `Tabs` component
- Include Air UI kit styles for the sandbox

## [1.4.5] - 2021-03-22
### Fixed
- Dropdown now correctly sets `selected` on the selected option

## [1.4.4] - 2021-03-01
### Changed
- Updated color of `LoadingSpinner` and `LoadingDots` to use NVIDIA green

## [1.4.3] - 2021-01-21
### Added
`disabled` prop for Dropdown

## [1.4.2] - 2021-01-15
### Added
- Alternate menu styles for Dropdown

## [1.4.1] - 2021-01-11
### Added
`maxHeight` prop for Dropdown

## [1.4.0] - 2020-08-10
### Added
Toast component

## [1.3.0] - 2020-07-13
### Added
Loading Spinner component

## [1.2.1] - 2020-06-17
### Added
updatePlaceholderOnSelection prop for Dropdown component

## [1.2.0] - 2020-06-11
### Added
Loading Dots component

## [1.1.0] - 2020-03-11
### Fixed
#1 - Converted to Javascript to fix production project builds

## [1.0.1] - 2020-03-04
### Changed
Converted Dropdown component to use div tags instead of select/option for easier styling

## [1.0.0] - 2020-03-02
### Added
Dropdown component
