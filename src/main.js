/**
 * Entrypoint for building component library published as an NPM module
 */

import * as components from './components/index';

const ComponentLibrary = {
    install: Vue => {
        Object.keys(components).forEach(name => {
            Vue.component(components[name].name, components[name]);
        });
    },
};

if (typeof window !== 'undefined' && window.Vue) {
    window.Vue.use(ComponentLibrary);
}

export default ComponentLibrary;
