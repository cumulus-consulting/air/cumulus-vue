# LoadingDots

Animated loading dots

## Tag

```
<cumulus-loading-dots/>
```

## Classes
- `cumulus-loading-dots` The main component container
- `cumulus-dot` A generic dot
- `cumulus-dot1` The 1st dot
- `cumulus-dot2` The 2nd dot
- `cumulus-dot3` The 3rd dot
