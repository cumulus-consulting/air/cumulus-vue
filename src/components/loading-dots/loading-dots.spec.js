import { expect } from 'chai';
import { shallowMount } from '@vue/test-utils';
import CumulusLoadingDots from './loading-dots.vue';

describe('LoadingDots', () => {
    let wrapper;

    before(async () => {
        wrapper = shallowMount(CumulusLoadingDots);
    });

    it('mounts', () => {
        expect(wrapper).to.be.ok;
    });
});
