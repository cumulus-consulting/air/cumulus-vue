# Tabs

A set of clickable and closable tabs, with a menu to add more tabs 

## Tag

```
<cumulus-tabs>
    <cumulus-tab :title="'Tab 1'" />
</cumulus-tabs>
```

## Props

### Required

### Optional
- `addOptions` (Array) A list of other tabs that can be opened
- `allowClose` (Boolean) Allow tabs to be closed (default: `true`)
- `dropdownMaxHeight` (String) Sets `max-height` for the add menu's dropdown (default: `100%`)
- `id` (String) `id` attribute of the tabs container (default: `CumulusTabs`)

## Events
- `tabClicked`: Emits the `id` of the clicked tab
- `tabClosed`: Emits the `id` of the closed tab

## Classes
- `c-tabs` The parent container of all the tabs
- `c-tabs-add-container` Container for the add tab button
- `c-tab` Individual tab
- `active-tab` Applied to the currently active tab
- `c-tab-close-btn` Close button
- `c-tab-title` Tab name/label
