import { expect } from 'chai';
import { createLocalVue, mount } from '@vue/test-utils';
import CumulusTab from './tab.vue';
import CumulusTabs from './tabs.vue';

describe('Tabs', () => {
    let wrapper;
    let tabs;
    let localVue;
    const defaultTab = { default: '<cumulus-tab title="Tab 1" />' };

    beforeEach(async () => {
        localVue = createLocalVue();
        localVue.component('cumulus-tab', CumulusTab);
        wrapper = mount(CumulusTabs, {
            localVue,
            propsData: {
                addOptions: [
                    {
                        display: 'Tab 1',
                        value: 'tab1',
                        componentOptions: {
                            propsData: { title: 'Tab 1' },
                        },
                    },
                    {
                        display: 'Tab 2',
                        value: 'tab2',
                        componentOptions: {
                            propsData: { title: 'Tab 2' },
                        },
                    },
                ],
                allowClose: true,
                dropdownMaxHeight: '200px',
                id: 'foo',
            },
            slots: defaultTab,
        });
        tabs = wrapper.find('.c-tabs');
    });

    it('renders "id" attribute', () => {
        expect(tabs.attributes().id).to.eq('foo');
    });

    it('can close tabs', async () => {
        tabs.find('.c-tab .c-tab-close-btn').trigger('click');
        await wrapper.vm.$nextTick();
        expect(wrapper.findAll('.c-tab').length).to.eq(0);
    });

    it('enforces allowClose', () => {
        const testWrapper = mount(CumulusTabs, {
            localVue,
            propsData: { addOptions: [], allowClose: false },
            slots: defaultTab,
        });
        expect(testWrapper.findAll('.c-tab-close-btn').length).to.eq(0);
    });

    it('can add a tab', async () => {
        tabs.find('.c-tabs-add-btn').trigger('click');
        await wrapper.vm.$nextTick();
        tabs.findAll('.dropdown-options .dropdown-option').at(1).trigger('click');
        await wrapper.vm.$nextTick();
        expect(tabs.findAll('.c-tab').at(1).html()).to.include('Tab 2');
    });

    it('sets dropdown max height', async () => {
        tabs.find('.c-tabs-add-btn').trigger('click');
        await wrapper.vm.$nextTick();
        expect(tabs.find('.dropdown-options').attributes().style).to.eq('max-height: 200px;');
    });
});
