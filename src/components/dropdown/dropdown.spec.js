import { expect } from 'chai';
import { shallowMount } from '@vue/test-utils';
import CumulusDropdown from './dropdown.vue';

describe('Dropdown', () => {
    let wrapper;
    let dropdown;

    context('single select', () => {
        before(async () => {
            wrapper = shallowMount(CumulusDropdown, {
                propsData: {
                    id: 'foo',
                    options: [
                        { display: 'Option 1', value: 'opt1' },
                        { display: 'Option 2', value: 'opt2', selected: true },
                    ],
                },
            });
            dropdown = wrapper.find('.dropdown');
        });

        beforeEach(async () => {
            if (!wrapper.find('.dropdown-options').exists()) {
                dropdown.trigger('click');
                await wrapper.vm.$nextTick();
            }
        });

        it('renders "id" attribute', () => {
            expect(dropdown.attributes().id).to.eq('foo');
        });

        it('renders all options', () => {
            const options = wrapper.findAll('.dropdown-options .dropdown-option');
            expect(options.at(0).text()).to.eq('Option 1');
            expect(options.at(0).attributes().id).to.eq('option-Option 1');
            expect(options.at(0).attributes().value).to.eq('opt1');
            expect(options.at(1).text()).to.eq('Option 2');
            expect(options.at(1).attributes().id).to.eq('option-Option 2');
            expect(options.at(1).attributes().value).to.eq('opt2');
        });

        it('pre-selects an option', () => {
            const selected = wrapper.find('.selected-option');
            expect(selected.text()).to.eq('Option 2');
        });

        it('emits a dropdownValueChanged event on selection', async () => {
            const options = wrapper.findAll('.dropdown-options .dropdown-option');
            options.at(0).trigger('click');
            await wrapper.vm.$nextTick();
            expect(wrapper.emitted('dropdownValueChanged')[0][0]).to.eq('opt1');
            expect(wrapper.emitted('dropdownValueChanged')[0][1]).to.be.an.instanceOf(MouseEvent);
        });

        it('emits open and close events', async () => {
            dropdown.trigger('click');
            await wrapper.vm.$nextTick();
            expect(wrapper.emitted('dropdownClosed')[0][0]).to.be.an.instanceOf(MouseEvent);
            dropdown.trigger('click');
            await wrapper.vm.$nextTick();
            expect(wrapper.emitted('dropdownOpened')[0][0]).to.be.an.instanceOf(MouseEvent);
        });

        it('auto-selects the first option by default', async () => {
            const testWrapper = shallowMount(CumulusDropdown, {
                propsData: {
                    options: [
                        { display: 'Option 1', value: 'opt1' },
                        { display: 'Option 2', value: 'opt2' },
                    ],
                },
            });
            testWrapper.trigger('click');
            await testWrapper.vm.$nextTick();

            const selected = wrapper.find('.selected-option');
            expect(selected.text()).to.eq('Option 1');
        });

        it('closes the dropdown when clicking outside', async () => {
            expect(wrapper.find('.dropdown-options').exists()).to.eq(true);
            document.body.click();
            await wrapper.vm.$nextTick();
            expect(wrapper.find('.dropdown-options').exists()).to.eq(false);
        });

        it('displays a placeholder', () => {
            const testWrapper = shallowMount(CumulusDropdown, {
                propsData: {
                    placeholder: 'foo',
                    options: [
                        { display: 'Option 1', value: 'opt1' },
                        { display: 'Option 2', value: 'opt2' },
                    ],
                },
            });

            const selected = testWrapper.find('.selected-option');
            expect(selected.text()).to.eq('foo');
        });

        it('respects updatePlaceholderOnSelection', async () => {
            const testWrapper = shallowMount(CumulusDropdown, {
                propsData: {
                    placeholder: 'foo',
                    options: [
                        { display: 'Option 1', value: 'opt1' },
                        { display: 'Option 2', value: 'opt2' },
                    ],
                    updatePlaceholderOnSelection: false,
                },
            });
            testWrapper.find('.dropdown').trigger('click');
            await wrapper.vm.$nextTick();
            const options = testWrapper.findAll('.dropdown-options .dropdown-option');
            options.at(0).trigger('click');
            await testWrapper.vm.$nextTick();
            const selected = testWrapper.find('.selected-option');
            expect(selected.text()).to.eq('foo');
        });

        it('has a maxHeight prop', async () => {
            const testWrapper = shallowMount(CumulusDropdown, {
                propsData: {
                    maxHeight: '40px',
                    placeholder: 'foo',
                    options: [
                        { display: 'Option 1', value: 'opt1' },
                        { display: 'Option 2', value: 'opt2' },
                    ],
                    updatePlaceholderOnSelection: false,
                },
            });
            testWrapper.find('.dropdown').trigger('click');
            await wrapper.vm.$nextTick();
            const options = testWrapper.find('.dropdown-options');
            expect(options.element.style.maxHeight).to.eq('40px');
        });

        it('renders an alternate dropdown menu style', () => {
            const altHtml = '<a id="alt-menu-link">Click Here</a>';
            const testWrapper = shallowMount(CumulusDropdown, {
                propsData: {
                    altMenuComponent: true,
                    options: [
                        { display: 'Option 1', value: 'opt1' },
                        { display: 'Option 2', value: 'opt2' },
                    ],
                },
                slots: {
                    default: altHtml,
                },
            });
            expect(testWrapper.html()).not.to.contain('class="dropdown"');
            expect(testWrapper.html()).to.contain('id="alt-menu-link">Click Here</a>');
        });

        it('can disable the dropdown', async () => {
            const testWrapper = shallowMount(CumulusDropdown, {
                propsData: {
                    disabled: true,
                    options: [
                        { display: 'Option 1', value: 'opt1' },
                        { display: 'Option 2', value: 'opt2' },
                    ],
                },
            });
            testWrapper.find('.dropdown').trigger('click');
            await wrapper.vm.$nextTick();
            expect(testWrapper.find('.dropdown-options').exists()).to.eq(false);
        });

        it('updates `selected` property on selection', async () => {
            const options = wrapper.findAll('.dropdown-options .dropdown-option');
            options.at(0).trigger('click');
            await wrapper.vm.$nextTick();
            expect(wrapper.vm.options[0].selected).to.eq(true);
            expect(wrapper.vm.options[1].selected).to.eq(false);
        });
    });

    context('multiselect', () => {
        beforeEach(async () => {
            wrapper = shallowMount(CumulusDropdown, {
                propsData: {
                    id: 'foo',
                    multiselect: true,
                    options: [
                        { display: 'Option 1', value: 'opt1', selected: false },
                        { display: 'Option 2', value: 'opt2', selected: false },
                    ],
                },
            });
            dropdown = wrapper.find('.dropdown');
            dropdown.trigger('click');
        });

        it('can select multiple options', async () => {
            const options = wrapper.findAll('.dropdown-options .dropdown-option input');
            options.at(0).trigger('click');
            await wrapper.vm.$nextTick();
            options.at(1).trigger('click');
            await wrapper.vm.$nextTick();
            expect(wrapper.vm.options[0].selected).to.eq(true);
            expect(wrapper.vm.options[1].selected).to.eq(true);
        });

        it('emits dropdownMultiselectChanged on selection', async () => {
            const options = wrapper.findAll('.dropdown-options .dropdown-option input');
            options.at(0).trigger('click');
            await wrapper.vm.$nextTick();
            expect(wrapper.emitted('dropdownMultiselectChanged')[0][0])
                .to.deep.eq(wrapper.vm.options[0]);
        });

        it('can select all options', async () => {
            const action = wrapper.find('.dropdown-actions a');
            expect(action.text()).to.eq('Select All');
            action.trigger('click');
            await wrapper.vm.$nextTick();
            expect(wrapper.vm.options[0].selected).to.eq(true);
            expect(wrapper.vm.options[1].selected).to.eq(true);
            expect(wrapper.emitted()).to.have.property('selectedAll');
        });

        it('can unselect all options', async () => {
            const action = wrapper.find('.dropdown-actions a');
            action.trigger('click');
            await wrapper.vm.$nextTick();
            expect(action.text()).to.eq('Unselect All');
            action.trigger('click');
            await wrapper.vm.$nextTick();
            expect(wrapper.vm.options[0].selected).to.eq(false);
            expect(wrapper.vm.options[1].selected).to.eq(false);
            expect(wrapper.emitted()).to.have.property('unselectedAll');
        });
    });
});
