# Dropdown

A dropdown component made of `div` tags. `<select>` and `<option>` tags are purposely not used to provide maximum flexibility in styling.

## Tag

```
<cumulus-dropdown/>
```

## Props

### Required
- `options` (Array) A list of options for the dropdown in the following format:
```
[
    {
        classes: "my-class",
        display: "Option 1",
        selected: true,
        value: "opt1"
    },
    ...
]
```

    - `classes` (string) Custom classes applied to the dropdown option (optional)
    - `display` (string) The option text to show in the dropdown
    - `selected` (boolean) Automatically set this option to be auto-selected (optional)
    - `value` (string) The option value to use when selected

### Optional
- `altMenuComponent` (boolean) If true, the dropdown will be rendered with an alternate menu component (see [Alternate Menu Styles](#alternate-menu-styles))
- `disabled` (boolean) If true, clicking the dropdown has no effect (default: `false`)
- `id` (string) An `id` attribute assigned to the `<select>` tag
- `maxHeight` (string) Value for the dropdown menu's `max-height` style property (default: `100%`)
- `multiselect` (boolean) Allow selecting multiple values from the dropdown (default: `false`)
- `placeholder` (string) Placeholder text to show in the dropdown menu until the user selects something
- `updatePlaceholderOnSelection` (boolean) If true, the dropdown's placeholder text is updated to show the currently selected option (default: true)

## Events
- `dropdownClosed`: Emits the click event when closing the dropdown
- `dropdownOpened`: Emits the click event when opening the dropdown
- `dropdownValueChanged`: Emits the selected option's `value` when the user changes the dropdown's value
- `dropdownMultiselectChanged`: Emits the selected option when the user selects it in a multiselect dropdown
- `selectedAll`: All multiselect options were selected
- `unselectedAll`: All multiselect options were unselected

## Event Handlers
- When the user clicks anyhwere outside of the dropdown, the dropdown is automatically closed

## Classes
- `alt-dropdown` The alternate dropdown menu
- `alt-dropdown-open` Set only when the alternate dropdown menu is open
- `dropdown` The main dropdown menu
- `dropdown-actions` The actions menu for the dropdown
- `dropdown-open` Set only when the dropdown menu is open
- `dropdown-options` The container for all menu item
- `dropdown-option` An individual menu item
- `selected-option` The selected menu item (always displayed in the menu)

## Alternate Menu Styles

By default, the component is rendered with a traditional dropdown menu. To use an alternate menu style (for example, an SVG icon or simple hyperlink), pass the alternate menu via the component's innerHTML value and set `altMenuComponent` to `true`.

```
<cumulus-dropdown :altMenuComponent="true" :options="myOptions">
    <a>Click here to open dropdown</a>
</cumulus-dropdown>
```
