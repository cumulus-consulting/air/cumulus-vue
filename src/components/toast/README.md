# Toast

A simple toast component that is displayed in the bottom corner.

## Tag

```
<cumulus-toast/>
```

## Props

### Optional
- `body` (string) The main body text of the toast
- `bodyUnsafeHTML` (string) Body text of the toast to be rendered as HTML (be careful)
- `title` (string) The title in the header of the toast

## Events
- `toast-closed`: Emitted when the toast's close button is clicked

## Classes
- `cumulus-toast` The main container
- `toast-header` The container for the title and the close button
- `toast-title` Title text
- `toast-close-btn` Close button
- `toast-body` Body text
