import { expect } from 'chai';
import { shallowMount } from '@vue/test-utils';
import CumulusToast from './toast.vue';

describe('Toast', () => {
    let wrapper;

    before(async () => {
        wrapper = shallowMount(CumulusToast, {
            propsData: {
                body: 'test body',
                title: 'test title',
            },
        });
    });

    it('renders "title" prop', () => {
        const title = wrapper.find('.toast-title');
        expect(title.text()).to.eq('test title');
    });

    it('renders "body" prop', () => {
        const body = wrapper.find('.toast-body');
        expect(body.text()).to.eq('test body');
    });

    it('renders "bodyUnsafeHTML" prop', () => {
        const testWrapper = shallowMount(CumulusToast, {
            propsData: {
                body: 'test body',
                bodyUnsafeHTML: '<div class="test">some html</html>',
                title: 'test title',
            },
        });
        const body = testWrapper.find('.toast-body .test');
        expect(body.text()).to.eq('some html');
    });

    it('emits when closed', () => {
        wrapper.find('.toast-close-btn').trigger('click');
        expect(wrapper.emitted('toast-closed')).not.to.be.undefined;
    });
});
