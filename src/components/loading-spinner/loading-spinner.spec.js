import { expect } from 'chai';
import { shallowMount } from '@vue/test-utils';
import CumulusLoadingSpinner from './loading-spinner.vue';

describe('LoadingSpinner', () => {
    let wrapper;

    before(async () => {
        wrapper = shallowMount(CumulusLoadingSpinner);
    });

    it('mounts', () => {
        expect(wrapper).to.be.ok;
    });
});
