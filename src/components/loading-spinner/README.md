# LoadingSpinner

Animated loading spinner

## Tag

```
<cumulus-loading-spinner/>
```

## Props

### Optional
- `fullScreen` (Boolean) When True, the spinner will take up the full screen and blur the background(default: false)
- `height` (String) A height to be passed to the spinner's style attribute (default: `120px`)
- `width` (String) A width to be passed to the spinner's style attribute (default: `120px`)

## Classes

- `spinner-container` Outer container for the component
- `spinner-circle-container` Container for the spinner circles
- `spinner-circle` A spinning circle
- `inner-circle` The inner-most circle of the spinner
- `full-screen` Set on the component container when the component's `fullScreen` prop is `true`
- `spinner-centered` Set on the circle container when the component's `fullScreen` prop is `true`
