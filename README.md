# cumulus-vue

The `cumulus-vue` library provides a common collection of reusable Vue.js components. These components are intended to be extremely lightweight, yet flexible enough to be used in any Vue.js-based project. They are purposely built with little to no styling, but have generic CSS classes included where appropriate to make it easy to add custom styling. The library is primarily tested with the [Air UI kit](https://gitlab.com/cumulus-consulting/air/air-ui-kit).

## Usage

### Install the library
```
npm install git+ssh://gitlab.com:cumulus-consulting/features/cumulus-vue.git
```

### Import and `Vue.use` the library in your app
```
import CumulusVue from '@cumulus/cumulus-vue';
import '@cumulus/cumulus-vue/lib/cumulus-vue.css';

Vue.use(CumulusVue);
```

### Use components in your app
```
<cumulus-dropdown :id="'foo'" :options="myOptions"/>
```

## Development

### Project setup
```
npm install
```

### Local testing and playing in the sandbox

First, compile the project and enable hot-reload for live testing:
```
npm start
```

Next, browse to http://localhost:8080/ to view the live sandbox for each component

### Build package for publishing
```
npm run build
```

### Unit testing
```
npm test
```

### Lint and fix files
```
npm run lint
```
